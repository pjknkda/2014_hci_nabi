git filter-branch -f --commit-filter '
        if [ "$GIT_AUTHOR_EMAIL" != "pjknkda@gmail.com" ];
        then
                GIT_AUTHOR_NAME="Jungkook Park";
                GIT_AUTHOR_EMAIL="pjknkda@gmail.com";
                GIT_COMMITTER_NAME="Jungkook Park";
                GIT_COMMITTER_EMAIL="pjknkda@gmail.com";
                git commit-tree "$@";
        else
                git commit-tree "$@";
        fi' HEAD
		
git filter-branch -f --commit-filter '
        if [ "$GIT_COMMITTER_EMAIL" != "pjknkda@gmail.com" ];
        then
                GIT_AUTHOR_NAME="Jungkook Park";
                GIT_AUTHOR_EMAIL="pjknkda@gmail.com";
                GIT_COMMITTER_NAME="Jungkook Park";
                GIT_COMMITTER_EMAIL="pjknkda@gmail.com";
                git commit-tree "$@";
        else
                git commit-tree "$@";
        fi' HEAD