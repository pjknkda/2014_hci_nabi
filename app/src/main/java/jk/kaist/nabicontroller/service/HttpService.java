package jk.kaist.nabicontroller.service;

import android.os.Handler;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import jk.kaist.nabicontroller.app.MainActivity;

/**
 * Created by Jungkook Park on 2014-05-20.
 */
public class HttpService {
    private static final String TAG = "HttpService";

    private static final String UserAgent = "NABI-Controller";

    public static boolean GETHttpResponse(String url, String method, List<NameValuePair> params, Handler handler)
    {
        String responseStr;

        try {
            URL urlObj = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();

            conn.setRequestMethod(method);
            conn.setRequestProperty("User-Agent", UserAgent);
            conn.setConnectTimeout(5000);
            conn.setReadTimeout(3000);
            conn.setUseCaches(false);

            if (params != null) {
                String urlParams = URLEncodedUtils.format(params, "utf-8");
                DataOutputStream outputStream = new DataOutputStream(conn.getOutputStream());
                outputStream.writeBytes(urlParams);
                outputStream.flush();
                outputStream.close();
            }

            int responseCode = conn.getResponseCode();
            if (responseCode < 200 || 300 <= responseCode) {
                Log.e(TAG, "HttpRequestJson failed: responseCode " + responseCode);
                return false;
            }

            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuffer response = new StringBuffer();
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }

            responseStr = response.toString();
        } catch (IOException e) {
            Log.e(TAG, "HttpRequestJson failed", e);
            return false;
        }

        handler.obtainMessage(MainActivity.HTTP_MESSAGE_RECV, responseStr).sendToTarget();
        return true;
    }
}
