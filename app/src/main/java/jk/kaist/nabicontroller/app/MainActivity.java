package jk.kaist.nabicontroller.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import jk.kaist.nabicontroller.service.BluetoothService;
import jk.kaist.nabicontroller.service.HttpService;


public class MainActivity extends Activity {
    // Debugging
    private static final String TAG = "Main";

    // Intent request code
    public static final int REQUEST_CONNECT_DEVICE = 1;
    public static final int REQUEST_ENABLE_BT = 2;

    // Main Activity Handler Bluetooth message type code
    public static final int BT_MESSAGE_CONN = 1;
    public static final int BT_MESSAGE_DCON = 2;
    public static final int BT_MESSAGE_RECV = 3;
    public static final int BT_MESSAGE_SEND = 4;

    // Main Activity Handler HTTP message type code
    public static final int HTTP_MESSAGE_RECV = 101;

    // Server Configuration
    private static final String SERVER_URL = "http://alter.clude.kr:14472/servo/get";

    // Servo Configuration
    private static final int SERVO_COUNT = 4;

    // Layout
    private CheckBox connectionCheckBox;
    private Button connectButton;
    private CheckBox serverConnectionCheckBox;
    private Button serverConnectButton;
    private EditText servo1EditText;
    private EditText servo2EditText;
    private EditText servo3EditText;
    private EditText servo4EditText;
    private Button sendButton;
    private ListView messageListView;

    private BluetoothService btService = null;
    private ArrayList<String> messageList;
    private ArrayAdapter<String> messageListAdapter;

    private Timer serverCommunicationTimer = null;
    private boolean serverCommunicationWorking = false;
    private int serverDataIdx = -1;

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.d(TAG, "Message arrived");
            switch (msg.what)
            {
                /* Bluetooth message */
                case BT_MESSAGE_CONN:
                    connectionCheckBox.setChecked(true);
                    addListItemWithTimestamp("Connected");
                    break;
                case BT_MESSAGE_DCON:
                    connectionCheckBox.setChecked(false);
                    addListItemWithTimestamp("Disconnected");
                    break;
                case BT_MESSAGE_RECV:
                    String line = (String)msg.obj;
                    addListItemWithTimestamp(line);
                    break;
                case BT_MESSAGE_SEND:
                    byte[] buffer = (byte[])msg.obj;
                    btService.write(buffer);
                    break;

                /* HTTP message */
                case HTTP_MESSAGE_RECV:
                    String response = (String)msg.obj;
                    try {
                        JSONObject jsonObj = new JSONObject(response);
                        String result = jsonObj.getString("result");
                        if (!result.equals("ok"))
                            return;

                        int dataIdx = jsonObj.getInt("idx");
                        if (serverDataIdx < dataIdx) {
                            serverDataIdx = dataIdx;
                            addListItemWithTimestamp("New data is arrived from the server.");

                            String anglesStr = jsonObj.getString("angles_str");
                            String[] angles = anglesStr.split(",");
                            if (angles.length < SERVO_COUNT)
                                return;

                            EditText[] angleEditTexts = {servo1EditText, servo2EditText, servo3EditText, servo4EditText};
                            for (int i = 0; i < SERVO_COUNT; i++) {
                                angleEditTexts[i].setText(angles[i]);
                            }

                            sendServoAngles();
                        }
                    } catch (JSONException e) {
                        Log.e(TAG, "Invalid JSON response", e);
                    }
                    break;
            }
        }
    };

    private class serverDataGetTask extends TimerTask {
        @Override
        public void run() {
            if (!HttpService.GETHttpResponse(SERVER_URL, "GET", null, mHandler)) {
                serverCommunicationTimer.cancel();
                serverCommunicationWorking = false;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        serverConnectionCheckBox.setChecked(serverCommunicationWorking);
                        addListItemWithTimestamp("Can't connect to the server.");
                    }
                });
            }
        }
    }

    private void addListItemWithTimestamp(String content) {
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss.SSS");
        String dateString = formatter.format(System.currentTimeMillis());
        String formattedContent = "[" + dateString +  "] " + content;

        messageList.add(formattedContent);
        messageListAdapter.notifyDataSetChanged();
    }

    private void sendServoAngles() {
        ArrayList<Byte> buffer = new ArrayList<Byte>();
        buffer.add((byte)'J');

        EditText[] angleEditTexts = {servo1EditText, servo2EditText, servo3EditText, servo4EditText};
        for (int i = 0; i < SERVO_COUNT; i++) {
            int angle = Integer.parseInt(angleEditTexts[i].getText().toString());
            buffer.add((byte) ((angle >> 7) & 127));
            buffer.add((byte) (angle & 127));
        }

        byte[] primBuffer = new byte[buffer.size()];
        for (int i = buffer.size() - 1; i >= 0; i--)
            primBuffer[i] = buffer.get(i);
        mHandler.obtainMessage(BT_MESSAGE_SEND, primBuffer).sendToTarget();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        connectionCheckBox = (CheckBox) findViewById(R.id.connection_checkbox);
        connectButton = (Button) findViewById(R.id.connect_button);
        serverConnectionCheckBox = (CheckBox) findViewById(R.id.server_connection_checkbox);
        serverConnectButton = (Button) findViewById(R.id.server_connect_button);
        servo1EditText = (EditText) findViewById(R.id.servo1_edittext);
        servo2EditText = (EditText) findViewById(R.id.servo2_edittext);
        servo3EditText = (EditText) findViewById(R.id.servo3_edittext);
        servo4EditText = (EditText) findViewById(R.id.servo4_edittext);
        sendButton = (Button) findViewById(R.id.send_button);
        messageListView = (ListView) findViewById(R.id.message_listView);

        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btService.getState() == BluetoothService.STATE_CONNECTED) {
                    btService.stop();
                } else {
                    if (btService.getDeviceState()) {
                        btService.enableBluetooth();
                    } else {
                        Toast.makeText(getApplicationContext(), "Do not support Bluetooth.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        serverConnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (serverCommunicationWorking) {
                    serverCommunicationTimer.cancel();
                    serverCommunicationWorking = false;
                    serverCommunicationTimer = null;
                } else {
                    serverCommunicationTimer = new Timer();
                    serverCommunicationTimer.schedule(new serverDataGetTask(), 0, 500);
                    serverCommunicationWorking = true;
                }
                serverConnectionCheckBox.setChecked(serverCommunicationWorking);
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendServoAngles();
            }
        });

        if (btService == null)
            btService = new BluetoothService(this, mHandler);

        if (messageList == null)
            messageList = new ArrayList<String>();

        messageListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, messageList);
        messageListView.setAdapter(messageListAdapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_info) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult " + resultCode);

        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    btService.getDeviceInfo(data);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    btService.scanDevice();
                } else {
                    Log.d(TAG, "Bluetooth is not enabled");
                }
                break;
        }
    }

    private static long backPressed = 0;
    @Override
    public void onBackPressed(){
        if (backPressed + 2000 > System.currentTimeMillis()){
            super.onBackPressed();
            if (serverCommunicationTimer != null) {
                serverCommunicationTimer.cancel();
            }
        }
        else{
            Toast.makeText(getBaseContext(), "Press once again to exit.", Toast.LENGTH_SHORT).show();
            backPressed = System.currentTimeMillis();
        }
    }
}
